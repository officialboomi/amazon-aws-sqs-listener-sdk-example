package com.boomi.connector.awssqslistener;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.util.BaseConnector;
import com.boomi.connector.util.listen.UnmanagedListenConnector;
import com.boomi.connector.util.listen.UnmanagedListenOperation;

public class AWSSQSListenerConnector extends UnmanagedListenConnector {

    @Override
    public Browser createBrowser(BrowseContext context) {
        return new AWSSQSListenerBrowser(createConnection(context));
    }    

    @Override
    protected Operation createGetOperation(OperationContext context) {
        return null;
    }
   
    private AWSSQSListenerConnection createConnection(BrowseContext context) {
        return new AWSSQSListenerConnection(context);
    }

	@Override
	public UnmanagedListenOperation createListenOperation(OperationContext context) {
        return new AWSSQSUnmanagedListenOperation(context);
	}
}